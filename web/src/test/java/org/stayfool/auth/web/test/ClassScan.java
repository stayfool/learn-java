package org.stayfool.auth.web.test;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

import org.junit.Test;

public class ClassScan {

	@Test
	public void scanClass() {

		try {
			Enumeration<URL> urls = getClassLoader().getResources("");
			while (urls.hasMoreElements())
				System.out.println(urls.nextElement().getPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private ClassLoader getClassLoader() {
		ClassLoader cl = null;
		try {
			cl = Thread.currentThread().getContextClassLoader();
		} catch (Throwable ex) {
			// Cannot access thread context ClassLoader - falling back...
		}
		if (cl == null) {
			// No thread context class loader -> use class loader of this class.
			cl = ClassScan.class.getClassLoader();
			if (cl == null) {
				// getClassLoader() returning null indicates the bootstrap
				// ClassLoader
				try {
					cl = ClassLoader.getSystemClassLoader();
				} catch (Throwable ex) {
					// Cannot access system ClassLoader - oh well, maybe the
					// caller can live with null...
				}
			}
		}
		return cl;
	}
}
