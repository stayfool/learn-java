/**
 * Created by jych1 on 2016/1/6.
 */
$(document).ready(function () {

    // 初始化表格
    var table = $('.datatable').DataTable({
        ajax: {
            url: '/user/list'
        },
        columns: [
            {
                data: 'userName'
            }
            ,
            {
                data: 'createDate'
            }
            ,
            {
                data: 'isLocked'
            }]

    });

    // 表格添加选中
    table.on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    // 删除选中
    $('#deleteUser').click(function () {
        table.row('.selected').remove().draw(false);
    });
});
