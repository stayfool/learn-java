/**
 * Created by jych1 on 2016/1/2.
 */
$(
    $(".panel-body > .navbar-nav > li > a").click(
        function () {
            $.ajax({
                url: this.id,
                async: true,
                cache: false,
                success: function (html) {
                    $(".main-content").html(html);
                }
            });
        }
    )
)