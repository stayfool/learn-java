package org.stayfool.auth.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.stayfool.auth.common.util.StringUtil;

public class RequestUtil {

	public static final ModelAndView getModelViewWithBasePath(String viewName, HttpServletRequest request) {
		if (StringUtil.isNullOrEmptyWithTrim(viewName) || request == null)
			return null;

		Map<String, Object> model = new HashMap<>();
		model.put("basePath", getBasePath(request));
		return new ModelAndView(viewName, model);
	}

	public static final String getBasePath(HttpServletRequest request) {
		if (request == null)
			return null;
		return request.getServletContext().getContextPath();
	}

}
