package org.stayfool.auth.shiro;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.config.Ini;
import org.springframework.beans.factory.FactoryBean;
import org.stayfool.auth.service.AuthService;

/**
 * @author jiangych
 */
public class FilterChainDefinitionsOfUrl implements FactoryBean<Ini.Section> {

	@Resource(name = "authService")
	private AuthService authService;

	private String preFilterChainDefinitions = null;
	private String afterFilterChainDefinitions = null;

	public Ini.Section getObject() throws Exception {
		Ini ini = new Ini();
		// 加载前置拦截器
		if (!StringUtils.isBlank(preFilterChainDefinitions))
			ini.load(preFilterChainDefinitions);
		Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);

		// 加载数据库中的配置
		Map<String, String> rp = authService.getResourceAuthorization();
		System.out.println(rp);
		section.putAll(rp);

		// 加载后置拦截器
		if (afterFilterChainDefinitions != null) {
			ini.load(afterFilterChainDefinitions);
			section.putAll(ini.getSection(Ini.DEFAULT_SECTION_NAME));
		}
		return section;
	}

	public Class<?> getObjectType() {
		return Ini.Section.class;
	}

	public boolean isSingleton() {
		return true;
	}

	// 注入方法
	public final void setPreFilterChainDefinitions(String preFilterChainDefinitions) {
		this.preFilterChainDefinitions = preFilterChainDefinitions;
	}

	public final void setAfterFilterChainDefinitions(String afterFilterChainDefinitions) {
		this.afterFilterChainDefinitions = afterFilterChainDefinitions;
	}

}
