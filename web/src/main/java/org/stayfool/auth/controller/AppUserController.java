package org.stayfool.auth.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.stayfool.auth.service.AuthService;

/**
 * Created by jych1 on 2015/12/26.
 */
@Controller
public class AppUserController {

	private static final String basepath = "/user";

	@Resource(name = "authService")
	private AuthService authService;

	@ResponseBody
	@RequestMapping(value = basepath + "/add", method = RequestMethod.POST)
	public void add(String userName, String password) {
		System.out.println("someone is comming ; user add");
	}

	@ResponseBody
	@RequestMapping(value = basepath + "/update", method = RequestMethod.PUT)
	public void update(String userName, String password) {
		System.out.println("someone is comming; user update");
	}

	@ResponseBody
	@RequestMapping(value = basepath + "/delete", method = RequestMethod.DELETE)
	public void delete(String name) {
		System.out.println("someone is comming; user delete");
	}

	@ResponseBody
	@RequestMapping(value = basepath + "/list", method = RequestMethod.GET)
	public void list() {
		System.out.println("someone is comming; user query");
	}

	@ResponseBody
	@RequestMapping(value = basepath)
	public void userConfig(ModelAndView view) {

		System.out.println("someone is comming;user config");
	}
}
