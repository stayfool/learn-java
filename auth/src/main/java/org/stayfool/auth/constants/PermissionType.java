package org.stayfool.auth.constants;

public enum PermissionType {

    /**
     * SSL
     */
    SSL,

    /**
     * AUTHC
     */
    AUTHC;

    /**
     * retun true if the name is contained
     *
     * @param name
     * @return
     */
    public static boolean contaions(String name) {
        for (PermissionType p : PermissionType.values()) {
            if (p.name().equals(name))
                return true;
        }
        return false;
    }

}
