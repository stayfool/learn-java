package org.stayfool.auth.constants;

/**
 * Created by jych1 on 2016/3/2.
 */
public enum ResourceType {

	/**
	 * 导航菜单
	 */
	NAVIGATION,

	/**
	 * 页面
	 */
	PAGE,

	/**
	 * 页面内菜单
	 */
	MENU,

	/**
	 * 页面内按钮
	 */
	BUTTON,

	/**
	 * URL
	 */
	URL;
}
