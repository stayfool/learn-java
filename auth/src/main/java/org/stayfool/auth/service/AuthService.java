package org.stayfool.auth.service;

import java.util.List;
import java.util.Map;

import org.stayfool.auth.common.service.BaseService;
import org.stayfool.auth.entity.auth.AppUser;
import org.stayfool.auth.vo.ResourceTree;

public interface AuthService extends BaseService {

	/**
	 * 获取资源-权限对应列表
	 * 
	 * @return 资源-权限
	 */
	Map<String, String> getResourceAuthorization();

	/**
	 * 根据用户名查询用户
	 * 
	 * @param userName
	 *            用户名
	 * @return {@code AppUser} 用户信息
	 */
	AppUser getUserByName(String userName);

	/**
	 * 查询权限
	 * 
	 * @param userName
	 * @return
	 */
	List<String> getPermissions(String userName);

	/**
	 * 查询角色
	 * 
	 * @param userName
	 * @return
	 */
	List<String> getRoles(String userName);

	/**
	 * 获取导航菜单
	 * 
	 * @param userName
	 * @return
	 */
	List<ResourceTree> getNavigation(String userName);

	/**
	 * 把URL保存成resource，存在则放弃
	 * 
	 * @param url
	 */
	void saveIfAbsent(String url);
}
