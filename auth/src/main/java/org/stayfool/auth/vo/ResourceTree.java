package org.stayfool.auth.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResourceTree implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6468216466579030967L;

	private String code;
	private String name;
	private String url;
	private String type;
	private List<ResourceTree> subResource = new ArrayList<>(0);

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ResourceTree> getSubResource() {
		return subResource;
	}

	public void setSubResource(List<ResourceTree> subResource) {
		this.subResource = subResource;
	}

}
