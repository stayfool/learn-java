package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jych
 *
 */
@Entity
@Table(name = "RESOURCE_ROLE_MAP")
public class ResourceRoleMap extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4327794209766633185L;

	private AppResource resource;
	private AppRole role;

	@ManyToOne
	public AppResource getResource() {
		return resource;
	}

	public void setResource(AppResource resource) {
		this.resource = resource;
	}

	@ManyToOne
	public AppRole getRole() {
		return role;
	}

	public void setRole(AppRole role) {
		this.role = role;
	}

}
