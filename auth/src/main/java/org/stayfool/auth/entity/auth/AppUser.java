package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author jiangych
 *
 */
@Entity
@Table(name = "APP_USER")
public class AppUser extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1578549072653198912L;

	private String code;
	private String username;
	private String password;
	private String salt;
	private Boolean isLocked;
	private Set<UserPermissionMap> permissionMaps = new HashSet<>(0);
	private Set<UserRoleMap> roleMaps = new HashSet<>(0);

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	@OneToMany(mappedBy = "user")
	public Set<UserPermissionMap> getPermissionMaps() {
		return permissionMaps;
	}

	public void setPermissionMaps(Set<UserPermissionMap> permissionMaps) {
		this.permissionMaps = permissionMaps;
	}

	@OneToMany(mappedBy = "user")
	public Set<UserRoleMap> getRoleMaps() {
		return roleMaps;
	}

	public void setRoleMaps(Set<UserRoleMap> roleMaps) {
		this.roleMaps = roleMaps;
	}

}
