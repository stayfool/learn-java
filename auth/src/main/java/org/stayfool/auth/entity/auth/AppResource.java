package org.stayfool.auth.entity.auth;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jych
 *
 */
@Entity
@Table(name = "APP_RESOURCE")
public class AppResource extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -60293982383866259L;

	private String code;
	private String name;
	private String url;
	private String type;
	private String description;
	private AppResource parent;
	private Set<AppResource> children = new HashSet<>(0);
	private Set<ResourcePermissionMap> permissionMaps = new HashSet<>(0);
	private Set<ResourceRoleMap> roleMaps = new HashSet<>(0);

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@ManyToOne
	public AppResource getParent() {
		return parent;
	}

	public void setParent(AppResource parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	public Set<AppResource> getChildren() {
		return children;
	}

	public void setChildren(Set<AppResource> children) {
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "resource")
	public Set<ResourcePermissionMap> getPermissionMaps() {
		return permissionMaps;
	}

	public void setPermissionMaps(Set<ResourcePermissionMap> permissionMaps) {
		this.permissionMaps = permissionMaps;
	}

	@OneToMany(mappedBy = "resource")
	public Set<ResourceRoleMap> getRoleMaps() {
		return roleMaps;
	}

	public void setRoleMaps(Set<ResourceRoleMap> roleMaps) {
		this.roleMaps = roleMaps;
	}
}
