package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jych
 *
 */
@Entity
@Table(name = "RESOURCE_PERMISSION_MAP")
public class ResourcePermissionMap extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4327794209766633185L;

	private AppResource resource;
	private AppPermission permission;

	@ManyToOne
	public AppResource getResource() {
		return resource;
	}

	public void setResource(AppResource resource) {
		this.resource = resource;
	}

	@ManyToOne
	public AppPermission getPermission() {
		return permission;
	}

	public void setPermission(AppPermission permission) {
		this.permission = permission;
	}

}
