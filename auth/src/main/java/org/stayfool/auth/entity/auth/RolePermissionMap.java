package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jiangych
 *
 */
@Entity
@Table(name = "ROLE_PERMISSION_MAP")
public class RolePermissionMap extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7559505221940951415L;

	private AppRole role;
	private AppPermission permission;

	@ManyToOne
	public AppRole getRole() {
		return role;
	}

	public void setRole(AppRole role) {
		this.role = role;
	}

	@ManyToOne
	public AppPermission getPermission() {
		return permission;
	}

	public void setPermission(AppPermission permission) {
		this.permission = permission;
	}

}
