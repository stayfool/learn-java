package org.stayfool.auth.entity.auth;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.stayfool.auth.entity.BaseEntity;

/**
 * @author jiangych
 */
@Entity
@Table(name = "APP_PERMISSION")
public class AppPermission extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 2954086745267543620L;

	private String code;
	private String name;
	private String description;
	private AppPermission parent;
	private Set<AppPermission> children = new HashSet<>(0);
	private Set<UserPermissionMap> userMaps = new HashSet<>(0);
	private Set<RolePermissionMap> roleMaps = new HashSet<>(0);
	private Set<ResourcePermissionMap> resourceMaps = new HashSet<>(0);

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	@ManyToOne
	public AppPermission getParent() {
		return parent;
	}

	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	public Set<AppPermission> getChildren() {
		return children;
	}

	@OneToMany(mappedBy = "permission")
	public Set<UserPermissionMap> getUserMaps() {
		return userMaps;
	}

	@OneToMany(mappedBy = "permission")
	public Set<RolePermissionMap> getRoleMaps() {
		return roleMaps;
	}

	@OneToMany(mappedBy = "permission")
	public Set<ResourcePermissionMap> getResourceMaps() {
		return resourceMaps;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParent(AppPermission parent) {
		this.parent = parent;
	}

	public void setChildren(Set<AppPermission> children) {
		this.children = children;
	}

	public void setUserMaps(Set<UserPermissionMap> userMaps) {
		this.userMaps = userMaps;
	}

	public void setRoleMaps(Set<RolePermissionMap> roleMaps) {
		this.roleMaps = roleMaps;
	}

	public void setResourceMaps(Set<ResourcePermissionMap> resourceMaps) {
		this.resourceMaps = resourceMaps;
	}

}
