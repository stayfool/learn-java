package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author jiangych
 *
 */
@Entity
@Table(name = "APP_ROLE")
public class AppRole extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5436760605916589481L;

	private String code;
	private String name;
	private String description;
	private Set<UserRoleMap> userMaps = new HashSet<>(0);
	private Set<RolePermissionMap> permissionMaps = new HashSet<>(0);
	private Set<ResourceRoleMap> roleMaps = new HashSet<>(0);

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "role", fetch = FetchType.EAGER)
	public Set<RolePermissionMap> getPermissionMaps() {
		return permissionMaps;
	}

	public void setPermissionMaps(Set<RolePermissionMap> permissionMaps) {
		this.permissionMaps = permissionMaps;
	}

	@OneToMany(mappedBy = "role")
	public Set<UserRoleMap> getUserMaps() {
		return userMaps;
	}

	public void setUserMaps(Set<UserRoleMap> userMaps) {
		this.userMaps = userMaps;
	}

	@OneToMany(mappedBy = "role")
	public Set<ResourceRoleMap> getRoleMaps() {
		return roleMaps;
	}

	public void setRoleMaps(Set<ResourceRoleMap> roleMaps) {
		this.roleMaps = roleMaps;
	}

}
