package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jiangych
 *
 */
@Entity
@Table(name = "USER_PERMISSION_MAP")
public class UserPermissionMap extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7458472205238552236L;

	private AppUser user;
	private AppPermission permission;

	@ManyToOne
	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	@ManyToOne
	public AppPermission getPermission() {
		return permission;
	}

	public void setPermission(AppPermission permission) {
		this.permission = permission;
	}

}
