package org.stayfool.auth.entity.auth;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.stayfool.auth.entity.BaseEntity;

/**
 * 
 * @author jiangych
 *
 */
@Entity
@Table(name = "USER_ROLE_MAP")
public class UserRoleMap extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1914932726305506604L;

	private AppUser user;
	private AppRole role;

	@ManyToOne
	public AppUser getUser() {
		return user;
	}

	@ManyToOne
	public AppRole getRole() {
		return role;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public void setRole(AppRole role) {
		this.role = role;
	}

}
