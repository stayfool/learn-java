package org.stayfool.auth;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.stayfool.auth.common.service.BaseService;
import org.stayfool.auth.constants.ResourceType;
import org.stayfool.auth.entity.auth.AppPermission;
import org.stayfool.auth.entity.auth.AppResource;
import org.stayfool.auth.entity.auth.AppRole;
import org.stayfool.auth.entity.auth.ResourcePermissionMap;
import org.stayfool.auth.entity.auth.RolePermissionMap;
import org.stayfool.auth.service.AuthService;

@ContextConfiguration(locations = { "classpath:spring-hibernate-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class CollectDB {

	@Autowired
	BaseService baseService;

	@Autowired
	AuthService authService;

	// @Test
	public void collect() {
		AppRole role = new AppRole();
		role.setName("admin");
		baseService.save(role);

		AppPermission permission = new AppPermission();
		permission.setCode("user:add:*");
		permission.setName("添加用户");
		baseService.save(permission);

		AppResource parent = new AppResource();
		parent.setCode("user:config");
		parent.setName("用户管理");
		parent.setType(ResourceType.BUTTON.name());
		baseService.save(parent);

		AppResource resource = new AppResource();
		resource.setUrl("/user/add");
		resource.setName("添加用户");
		resource.setType(ResourceType.BUTTON.name());
		resource.setParent(parent);
		baseService.save(resource);

		resource = new AppResource();
		resource.setUrl("/user/update");
		resource.setName("编辑用户");
		resource.setType(ResourceType.BUTTON.name());
		resource.setParent(parent);
		baseService.save(resource);

		resource = new AppResource();
		resource.setUrl("/user/delete");
		resource.setName("删除用户");
		resource.setType(ResourceType.BUTTON.name());
		resource.setParent(parent);
		baseService.save(resource);

		resource = new AppResource();
		resource.setUrl("/user/list");
		resource.setName("查询用户");
		resource.setType(ResourceType.BUTTON.name());
		resource.setParent(parent);
		baseService.save(resource);

		RolePermissionMap rp = new RolePermissionMap();
		rp.setRole(role);
		rp.setPermission(permission);
		baseService.save(rp);

		ResourcePermissionMap rpm = new ResourcePermissionMap();
		rpm.setResource(resource);
		rpm.setPermission(permission);
		baseService.save(rpm);

	}

	// @Test
	public void subclass() {
		Class<AppRole> cls = AppRole.class;

		Arrays.asList(cls.getFields()).parallelStream().forEach(System.out::println);
		CountDownLatch c = new CountDownLatch(1);

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(10000);
						System.out.println(System.currentTimeMillis());
						c.countDown();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});
		t.start();
		try {
			c.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
