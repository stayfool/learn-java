package org.stayfool.auth;

import org.junit.Test;
import org.stayfool.auth.constants.ResourceType;

public class EnumValueOfTest {

	@Test
	public void test() {
		ResourceType r = ResourceType.valueOf("MENU");
		System.out.println(r);

		ResourceType r1 = ResourceType.valueOf(ResourceType.class, "MENU");
		System.out.println(r1);

		try {
			ResourceType r2 = ResourceType.valueOf("MENasdfU");
			System.out.println(r2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			ResourceType r3 = ResourceType.valueOf(ResourceType.class, "5646");
			System.out.println(r3);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
