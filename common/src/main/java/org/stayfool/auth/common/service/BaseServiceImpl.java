package org.stayfool.auth.common.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.stayfool.auth.common.dao.BaseDao;

/**
 * @author jiangych
 */
@Service("baseService")
@Transactional(rollbackFor = Throwable.class)
public class BaseServiceImpl implements BaseService {

	@Autowired
	private BaseDao baseDao;

	public Serializable save(Object entity) {
		return baseDao.save(entity);
	}

	public void update(Object entity) {
		baseDao.update(entity);
	}

	public void delete(Object entity) {
		baseDao.delete(entity);
	}

	public <T> T get(Class<T> cls, String uuid) {
		return baseDao.get(cls, uuid);
	}

	public <T> Map<T, Serializable> saveAll(Collection<T> entities) {
		return baseDao.saveAll(entities);
	}

	public void updateAll(Collection<Object> entities) {
		baseDao.updateAll(entities);
	}

	public void deleteAll(Collection<Object> entities) {
		baseDao.deleteAll(entities);
	}

	public <T> List<T> find(String hql) {
		return baseDao.find(hql);
	}

	public <T> List<T> find(String hql, Object... params) {
		return baseDao.find(hql, params);
	}

	public <T> List<T> find(String hql, int firstResult, int maxResult, Object... params) {
		return baseDao.find(hql, firstResult, maxResult, params);
	}

	public <T> T findUnique(String hql) {
		return baseDao.findUnique(hql);
	}

	public <T> T findUnique(String hql, Object... params) {
		return baseDao.findUnique(hql, params);
	}

	public <T> T findUnique(Class<T> cls, String uniqueProperty, Object value) {
		return baseDao.findUnique(cls, uniqueProperty, value);
	}

	public <T> List<T> findObj(String hql) {
		return baseDao.findObj(hql);
	}

	public <T> List<T> findObj(String hql, Object... params) {
		return baseDao.findObj(hql, params);
	}

	public void execute(String hql) {
		baseDao.execute(hql);
	}

	public void execute(String hql, Object... params) {
		baseDao.execute(hql, params);
	}

}
