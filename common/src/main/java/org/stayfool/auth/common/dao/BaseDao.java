package org.stayfool.auth.common.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

/**
 * Created by jych1 on 2016/3/9.
 */
public interface BaseDao {

	Serializable save(Object entity);

	void update(Object entity);

	void delete(Object entity);

	<T> T get(Class<T> cls, String uuid);

	<T> Map<T, Serializable> saveAll(Collection<T> entities);

	void updateAll(Collection<Object> entities);

	void deleteAll(Collection<Object> entities);

	<T> List<T> find(String hql);

	<T> List<T> find(String hql, Object... params);

	<T> List<T> find(String hql, int firstResult, int maxResult, Object... params);

	<T> T findUnique(String hql);

	<T> T findUnique(String hql, Object... params);

	<T> T findUnique(Class<T> cls, String uniqueProperty, Object value);

	<T> List<T> findObj(String hql);

	<T> List<T> findObj(String hql, Object... params);

	void execute(String hql);

	void execute(String hql, Object... params);

	Session getSession();
}
