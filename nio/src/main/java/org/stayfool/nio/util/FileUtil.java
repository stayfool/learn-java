package org.stayfool.nio.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileUtil {

	/**
	 * 读取文件到控制台
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void show(String path) throws FileNotFoundException, IOException {
		System.err.println("---------------------读取开始----------------------");
		BufferedReader r = new BufferedReader(new FileReader(new File(path)));
		String line = "";
		while ((line = r.readLine()) != null)
			System.out.println(line);

		r.close();
		System.err.println("---------------------读取完成----------------------");
	}

}
