package org.stayfool.nio.enums;

public enum FileAccess {
	/**
	 * read only
	 */
	READ_ONLY("r"),
	
	/**
	 * read and write
	 */
	READ_AND_WRITE("rw");

	private String code;

	FileAccess(String code) {
		this.code = code;
	}

	public String code() {
		return this.code;
	}
}
